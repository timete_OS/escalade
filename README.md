Ranking climbers and routes
===============



Ranking players in competitive games is often achieved using an Elo ranking. Climbing is not a competitive game, in the sense that no one is playing against one another.

However, some cimbers are "playing" against the same opponents, that is the same routes.

Thus, there must be a way to algorithmically create a ranking of climbers and routes, only based on the results of the ascents. (*les issues des expériences aléatoires*)


## Hypothesis of the model


Every climber has an intrisic level $m$, modeled by a normal distibution $\mathcal{N}(m, s)$

Every route $i$ has a fixed difficulty, modeled by a simple real number $d_i$.

When a climber attempt to climb a route, this is the result of the random experiment of sampling a number from the climber level distribution, and the climbers succeeds if this number is superior to the route difficulty $d_i$.


The goal is to find an algorithm that estimates those parameters for every climber and route.
